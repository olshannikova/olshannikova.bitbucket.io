function StepViewModel(stepNumber, types, ids, labelNames) {
    var self = this;
    self.stepNumber = stepNumber;
    self.fields = [];

    for (var i = 0; i < types.length; i += 1) {
        self.fields.push(new FieldViewModel(types[i], ids[i], labelNames[i]));
    }
    /*
    * Saves information to localStorage. Label's text is used as key
    */
    self.saveToLocalStorage = function() {
        var i;
        for (i = 0; i < self.fields.length; i += 1) {
            if (self.fields[i].value() !== "" && self.fields[i].type !== "password") {
                localStorage.setItem(self.fields[i].labelName, self.fields[i].value());
            }
        }
    }
    /*
    * Checks if all required input's of section are filled
    * {return} not filled input's index or -1 if everything is filled
    */
    self.allFilled = function() {
        var i;
        for (i = 0; i < self.fields.length; i += 1) {
            if (self.fields[i].isFilled() !== 1) {
                return i;
            }
        }
        return -1;
    }
}