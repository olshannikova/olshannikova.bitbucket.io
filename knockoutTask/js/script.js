(function(){
    var types = [["text", "text", "text", "number", "date"],
                ["email", "tel", "url"],
                ["text"],
                ["password", "password"]];
    var ids = [["lastName", "firstName", "middleName", "age", "birthDate"],
                ["email", "phone", "website"],
                ["nickname"],
                ["pass", "confirmPass"]];
    var labelNames = [["Фамилия", "Имя", "Отчество", "Возраст", "Дата рождения"],
                    ["Email", "Телефон", "Вебсайт"],
                    ["Никнейм"],
                    ["Пароль", "Подтвердите пароль"]];

    ko.applyBindings(new WizardViewModel(4, types, ids, labelNames));
})();