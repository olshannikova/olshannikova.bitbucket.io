$(document).ready(function() {
    $('#image1-container').prepend('<img id="image1" src="img/cat1.jpg">');
    $('#image2-container').prepend('<img class="image2" src="img/cat2.jpg">');
    $('#image2-container').after('<button>swap</button>');
    
    $('button').click(function() {
        var first = $('#image1-container').contents();
        var scnd = $('#image2-container').contents();
        $('#image1-container').append(scnd);
        $('#image2-container').append(first);
    })
    $('img').click(function() {
        $(this).width(this.clientWidth + 10);
    })
    $('img').hover(function() {
        $(this).toggleClass('img-border');
    })
});