"use strict";
/**
* Text Formatter object's constructor
*/
function TextFormatter() {
    this.cutTextLength = function(text, maxLength) {
        if (Number.isNaN(maxLength)) {
            return text;
        }
        return text.substr(0, maxLength);
    }
    this.splitText = function(text, delimiter, stringMaxNum) {
        var array = text.split(delimiter, stringMaxNum);
        text = "";
        for (var i = 0; i < array.length; i++)
            text += array[i] + "\n";
        return text;
    }
    this.formatType = function(text, fType, stringMaxNum) {
        if (isNaN(stringMaxNum))
            stringMaxNum = text.length;
        if (fType === undefined)
            return text;
        switch (fType) {
            case "word":
                return this.splitText(text, " ", stringMaxNum);
            case "symbol":
                return this.splitText(text, "", stringMaxNum);
            case "sentence":
                return this.splitText(text, ".", stringMaxNum);
            case "none":
                return text;
        }
    }
}

TextFormatter.format = function(text, maxLength, fType, stringMaxNum) {
    var tf = new TextFormatter();
    var temp = tf.cutTextLength(text, maxLength);
    temp = tf.formatType(temp, fType, stringMaxNum);
    return temp;
}