(function() {
    /*==============================ARRAY PROCESSING TOOL==============================*/
    /*==============================SUB SUM==============================*/
    var resultStr = "<table><tr><th>Массив</th><th>Макс. сумма за O(n^2)</th><th>Макс. сумма за O(n)</th></tr>";
    var testArrays = [[-1, 2, 3, -9],
                     [2, -1, 2, 3, -9],
                     [-1, 2, 3, -9, 11],
                     [-2, -1, 1, 2],
                     [100, -9, 2, -3, 5],
                     [1, 2, 3],
                     [-1, -2, -3]];
    for (var i = 0; i < testArrays.length; i++) {
        resultStr += "<tr><td>[" + testArrays[i].toString() + "]</td><td>" + ArrayProcessingTool.subSumOn2(testArrays[i])+ "</td><td>" + ArrayProcessingTool.subSumOn1(testArrays[i]) + "</td><tr>";
    }
    resultStr += "</table>"
    document.getElementById("sub-sum").innerHTML = resultStr;
    
    document.getElementById("sub-sum-button").onclick = function() {
        var array = document.getElementsByTagName("input")[0].value.split(' ');
        for (var i = 0; i < array.length; i++) {
            array[i] = parseFloat(array[i], 10);
        }
        document.getElementById("sub-sum-result").innerHTML = "<b>O(n):</b> " + ArrayProcessingTool.subSumOn1(array) + ",&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>O(n^2):</b> " + ArrayProcessingTool.subSumOn2(array);
    }
    /*==============================SEARCH==============================*/
    resultStr = "<table><tr><th>Массив</th><th>Max</th><th>Min</th><th>Median</th></tr>";
    testArrays = [[15, 1, -5, 100, 50, -10],
                 [2.3, 4, 5.5, -2, -100],
                 [-1, -2, -3]];
    for (var i = 0; i < testArrays.length; i++) {
        resultStr += "<tr><td>[" + testArrays[i].toString() + "]</td><td>" + ArrayProcessingTool.findMax(testArrays[i]) + "</td><td>" + ArrayProcessingTool.findMin(testArrays[i]) + "</td><td>" + ArrayProcessingTool.findMedian(testArrays[i]) + "</td><tr>";
    }
    resultStr += "</table>";
    document.getElementById("search").innerHTML = resultStr;
    
    document.getElementById("search-button").onclick = function() {
        var array = document.getElementsByTagName("input")[2].value.split(' ');
        for (var i = 0; i < array.length; i++) {
            array[i] = parseFloat(array[i], 10);
        }
        document.getElementById("search-result").innerHTML = "<b>Max:</b> " + ArrayProcessingTool.findMax(array) + ",&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Min:</b> " + ArrayProcessingTool.findMin(array) + ",&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Median:</b> " + ArrayProcessingTool.findMedian(array);
    }
    /*==============================SELECTION TASK==============================*/
    resultStr = "<table><tr><th>Массив</th><th>Возрастающая послед-ть макс. длины</th></tr>"
    testArrays = [[1, 3, 7, 4, 6, 7, 8, 1, 2, 5, 7, 8, 90, 1],
                 [1, 2, 3, 4, 5, 6]];
    for (var i = 0; i < testArrays.length; i++) {
        resultStr += "<tr><td>[" + testArrays[i].toString() + "]</td><td>[" + ArrayProcessingTool.ascSequence(testArrays[i]) + "]</td><tr>";
    }
    resultStr += "</table>";
    document.getElementById("selection-task").innerHTML = resultStr;
    
    document.getElementById("selection-button").onclick = function() {
        var array = document.getElementsByTagName("input")[4].value.split(' ');
        for (var i = 0; i < array.length; i++) {
            array[i] = parseFloat(array[i], 10);
        }
        document.getElementById("selection-result").innerHTML = "<b>Результат:</b> [" + ArrayProcessingTool.ascSequence(array) + "]";
    }
    
    /*==============================DATE DISPLAY FORMATTER==============================*/
    resultStr = "";
    resultStr += "'31102011' => " + DateDisplayFormatter.format('string', '31102011') + "<br>";
    resultStr += "'31102011' => " + DateDisplayFormatter.format('string', '31102011', true) + "<br>";
    resultStr += "('20130431', 'YYYYMMDD') => " + DateDisplayFormatter.format('string', '20130431', true, 'YYYYMMDD') + "<br>";
    resultStr += "('20130431', 'YYYYMMDD', 'MM-DD-YYYY') => " + DateDisplayFormatter.format('string', '20130431', false, 'YYYYMMDD', 'MM-DD-YYYY') + "<br>";
    document.getElementById("date-display-formatter").innerHTML = resultStr;
    
    document.getElementById("date-formatter-button").onclick = function() {
        var inputType;
        if (document.getElementById("ms").checked) {
            inputType = 'ms';
        } else inputType = 'string';
        var date = document.getElementById("inputDate").value;
        var displayMonthName = document.getElementById("displayMonthName").checked;
        var regularIn = document.getElementById("regularIn").value;
        if (regularIn.length == 0) {
            regularIn = undefined;
        }
        var regularOut = document.getElementById("regularOut").value;
        if (regularOut.length == 0) {
            regularOut = undefined;
        }
        document.getElementById("date-display-formatter-result").innerHTML = DateDisplayFormatter.format(inputType, date, displayMonthName, regularIn, regularOut);
    }
    
    /*==============================TEXT FORMATTER==============================*/
    var testStr = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sit amet consequat dui, at sollicitudin tellus. In vulputate sollicitudin dui eu suscipit.";
    resultStr = "<table><tr><th>Строка</th><th>Макс. размер строки</th><th>Тип форматирования</th><th>Макс. кол-во строк</th><th>Результат</th><tr>";
    resultStr += "<tr><td>" + testStr + "</td><td>-</td><td>-</td><td>-</td><td>" + TextFormatter.format(testStr) +"</td></tr>";
    resultStr += "<tr><td>" + testStr + "</td><td>20</td><td>-</td><td>-</td><td>" + TextFormatter.format(testStr, 20) +"</td></tr>";
    resultStr += "<tr><td>" + testStr + "</td><td>5</td><td>перенос по символу</td><td>-</td><td><pre>" + TextFormatter.format(testStr, 5, 'symbol') +"</pre></td></tr>";
    resultStr += "<tr><td>" + testStr + "</td><td>50</td><td>перенос по слову</td><td>-</td><td><pre>" + TextFormatter.format(testStr, 50, 'word') +"</pre></td></tr>";
    resultStr += "<tr><td>" + testStr + "</td><td>200</td><td>перенос по предложению</td><td>-</td><td><pre>" + TextFormatter.format(testStr, 200, 'sentence') +"</pre></td></tr>";
    resultStr += "<tr><td>" + testStr + "</td><td>50</td><td>перенос по слову</td><td>2</td><td><pre>" + TextFormatter.format(testStr, 50, 'word', 2) +"</pre></td></tr>";
    resultStr += "</table>";
    document.getElementById("text-formatter").innerHTML = resultStr;
    
    document.getElementById("text-formatter-button").onclick = function() {
        var text = document.getElementsByTagName("textarea")[0].value;
        var maxLength = parseInt(document.getElementById("string-max-size").value, 10);
        var stringMaxNum = parseInt(document.getElementById("string-max-num").value, 10);
        
        var opt = document.getElementById("format-type");
        var fType;
        switch (opt.options[opt.selectedIndex].value) {
            case "1":
                fType = "word";
                break;
            case "2":
                fType = "symbol";
                break;
            case "3":
                fType = "sentence";
                break;
            case "4":
                fType = "none";
                break;
        }
        document.getElementById("text-formatter-result").innerHTML = "<pre>" + TextFormatter.format(text, maxLength, fType, stringMaxNum) + "</pre>";
    }
    
    /*==============================STRING CALCULATOR==============================*/
    resultStr = "";
    testArrays = [["1", "2"],
                     ["-10", "20"],
                     ["2.5", "2"],
                     ["-3.5", "-1.5"]];
    for (var i = 0; i < testArrays.length; i++) {
        resultStr += testArrays[i][0] + " + " + testArrays[i][1] + " = " + StringCalculator.calc("+", testArrays[i][0], testArrays[i][1]) + "<br>";
        resultStr += testArrays[i][0] + " - " + testArrays[i][1] + " = " + StringCalculator.calc("-", testArrays[i][0], testArrays[i][1]) + "<br>";
        resultStr += testArrays[i][0] + " * " + testArrays[i][1] + " = " + StringCalculator.calc("*", testArrays[i][0], testArrays[i][1]) + "<br>";
        resultStr += testArrays[i][0] + " / " + testArrays[i][1] + " = " + StringCalculator.calc("/", testArrays[i][0], testArrays[i][1]) + "<br>";
    }
    document.getElementById("string-calculator").innerHTML = resultStr;
    
    document.getElementById("string-calculator-button").onclick = function() {
        var firstNum = document.getElementById("first-num").value;
        var secondNum = document.getElementById("second-num").value;
        var opt = document.getElementById("operation");
        var operation;
        switch (opt.options[opt.selectedIndex].value) {
            case "1":
                operation = "+";
                break;
            case "2":
                operation = "-";
                break;
            case "3":
                operation = "*";
                break;
            case "4":
                operation = "/";
                break;
        }
        document.getElementById("string-calculator-result").innerHTML = StringCalculator.calc(operation, firstNum, secondNum);
    }
    
    /*==============================ARRAY SORTER==============================*/
    resultStr = "<table><tr><th>Массив</th><th>Пузырьком</th><th>Вставками</th><th>Выбором</th><th>Быстрая</th></tr>";
    testArrays = [[10, -2, 4, 7, 45, 64, -19],
                 [-2, 5, 0, -6, 9, 10, 17],
                 [43, 65, 98, -34, 24, 87, 66, -1]];
    for (var i = 0; i < testArrays.length; i++) {
        resultStr += "<tr><td>[" + testArrays[i] + "]</td><td>[" + ArraySorter.bubbleSort(testArrays[i].slice(0)) + "]</td><td>[" + ArraySorter.insertionSort(testArrays[i].slice(0)) + "]</td><td>[" + ArraySorter.selectionSort(testArrays[i].slice(0)) + "]</td><td>[" + ArraySorter.quickSort(testArrays[i].slice(0)) + "]</td></tr>";
    }
    resultStr += "</table>";
    document.getElementById("array-sorter").innerHTML = resultStr;
    
    document.getElementById("array-sorter-button").onclick = function() {
        var array = document.getElementById("unsorted-array").value.split(' ');
        for (var i = 0; i < array.length; i++) {
            array[i] = parseFloat(array[i], 10);
        }
        var opt = document.getElementById("sort");
        switch (opt.options[opt.selectedIndex].value) {
            case "1":
                array = ArraySorter.bubbleSort(array);
                break;
            case "2":
                array = ArraySorter.insertionSort(array);
                break;
            case "3":
                array = ArraySorter.selectionSort(array);
                break;
            case "4":
                array = ArraySorter.quickSort(array);
                break;
            default:
                break;
        }
        document.getElementById("array-sorter-result").innerHTML = array;
    }
    
    /*==============================BINARY CONVERTER==============================*/
    resultStr = "";
    resultStr += "1000010101011<sub>2</sub> = " + NumericSystemConverter.binaryToDecimal([1,0,0,0,0,1,0,1,0,1,0,1,1]) + "<sub>10</sub><br>";
    resultStr += "1111111111<sub>2</sub> = " + NumericSystemConverter.binaryToDecimal([1,1,1,1,1,1,1,1,1,1]) + "<sub>10</sub><br>";
    resultStr += "10101010101<sub>2</sub> = " + NumericSystemConverter.binaryToDecimal([1,0,1,0,1,0,1,0,1,0,1]) + "<sub>10</sub><br>";
    resultStr += "4267<sub>10</sub> = " + NumericSystemConverter.decimalToBinary([4,2,6,7]) + "<sub>2</sub><br>";
    resultStr += "1023<sub>10</sub> = " + NumericSystemConverter.decimalToBinary([1,0,2,3]) + "<sub>2</sub><br>";
    resultStr += "1365<sub>10</sub> = " + NumericSystemConverter.decimalToBinary([1,3,6,5]) + "<sub>2</sub><br>";
    document.getElementById("binary-converter").innerHTML = resultStr;
    
    document.getElementById("binary-button").onclick = function() {
        var binNum = document.getElementById("binary").value.split('');
        for (var i = 0; i < binNum.length; i++) {
            binNum[i] = parseInt(binNum[i], 10);
        }
        document.getElementById("decimal-result").innerHTML = "В десятичной системе: " + NumericSystemConverter.binaryToDecimal(binNum) + "<br>";
    }
    document.getElementById("decimal-button").onclick = function() {
        var decNum = document.getElementById("decimal").value.split('');
        for (var i = 0; i < decNum.length; i++) {
            decNum[i] = parseInt(decNum[i], 10);
        }
        document.getElementById("binary-result").innerHTML = "В десятичной системе: " + NumericSystemConverter.decimalToBinary(decNum) + "<br>";
    }
    
    /*==============================CACHING CALCULATOR==============================*/
    var cachingCalculator;
    document.getElementById("caching-calculator-start").onclick = function() {
        var cacheSize = parseInt(document.getElementById("cache-size").value, 10);
        var isAddCaching = document.getElementById("isAddCaching").checked;
        var isSubCaching = document.getElementById("isSubCaching").checked;
        var isMulCaching = document.getElementById("isMulCaching").checked;
        var isDivCaching = document.getElementById("isDivCaching").checked;
        cachingCalculator = new CachingCalculator(cacheSize, isAddCaching, isSubCaching, isMulCaching, isDivCaching);
        document.getElementById("caching-calculator-button").removeAttribute("disabled");
    }
    document.getElementById("caching-calculator-button").onclick = function() {
        var firstCacheNum = parseInt(document.getElementById("first-cache-num").value, 10);
        var secondCacheNum = parseInt(document.getElementById("second-cache-num").value, 10);
        var opt = document.getElementById("cache-operation");
        var result;
        switch (opt.options[opt.selectedIndex].value) {
            case "1":
                operation = "+";
                result = cachingCalculator.add(firstCacheNum, secondCacheNum);
                break;
            case "2":
                operation = "-";
                result = cachingCalculator.sub(firstCacheNum, secondCacheNum);
                break;
            case "3":
                operation = "*";
                result = cachingCalculator.mul(firstCacheNum, secondCacheNum);
                break;
            case "4":
                operation = "/";
                result = cachingCalculator.div(firstCacheNum, secondCacheNum);
                break;
            default:
                result = "некорректная операция";
                break;
        }
        var cacheContains = cachingCalculator.getCache();
        var cache = "Кэш: ";
        for (var key in cacheContains) {
            cache += key + " = " + cacheContains[key] + "; ";
        }
        document.getElementById("caching-calculator-result").innerHTML = "Результат: " + result;
        document.getElementById("cache").innerHTML = cache;
    }
})();