"use strict";
/**
* Array processing object's constructor
*/
function ArrayProcessingTool() {}
/**
* Finds contiguous subarray which has maximum sum of elements
* complexity O(n^2)
* @param {number[]} array - array
* @return {number} maximum sum
*/
ArrayProcessingTool.subSumOn2 = function(array) {
    if (!Array.isArray(array)) {
        return -1;
    }
    var sum = 0;
    for (var i = 0; i < array.length; i++) {
        var localSum = 0;
        for (var j = i; j < array.length; j++) {
            localSum += array[j];
            if (localSum > sum)
                sum = localSum;
        }
    }
    return sum;
}
/**
* Finds contiguous subarray which has maximum sum of elements
* complexity O(n)
* @param {number[]} array - array
* @return {number} maximum sum
*/
ArrayProcessingTool.subSumOn1 = function(array) {
    if (!Array.isArray(array)) {
        return -1;
    }
    var sum = 0, partSum = 0;
    array.forEach(function(element, index, array) {
        partSum += element;
        if (partSum > sum)
            sum = partSum;
        if (partSum < 0)
            partSum = 0;
    });
    return sum;
}
/**
* Finds minimum of array
* @param {number[]} array - array
* @return {number} minimum element
*/
ArrayProcessingTool.findMin = function(array) {
    if (!Array.isArray(array)) {
        return -1;
    }
    var min = array[0];
    for (var i = 1; i < array.length; i++) {
        if (array[i] < min)
            min = array[i];
    }
    return min;
}
/**
* Finds maximum of array
* @param {number[]} array - array
* @return {number} maximum element
*/
ArrayProcessingTool.findMax = function(array) {
    if (!Array.isArray(array)) {
        return -1;
    }
    var max = array[0];
    for (var i = 1; i < array.length; i++) {
        if (array[i] > max)
            max = array[i];
    }
    return max;
}
/**
* Finds median of array
* @param {number[]} array - array
* @return {number} median
*/
ArrayProcessingTool.findMedian = function(array) {
    if (!Array.isArray(array)) {
        return -1;
    }
    array.sort( function(a, b) {return a - b;} );
    var half = ~~(array.length / 2);
    return array[half];
}
/**
* Finds the maximal ascending subsequence of array
* @param {number[]} array - array
* @return {number[]} array
*/
ArrayProcessingTool.ascSequence = function(array) {
    if (!Array.isArray(array)) {
        return -1;
    }
    var start = 0, length = 1, oldLength = 0, oldStart = -1;
    for (var i = 1; i < array.length; i++) {
        if (array[i] > array[i - 1])
            length++;
        else {
            if (oldLength < length) {
                oldLength = length;
                oldStart = start;
            }
            start = i;
            length = 1;
        }
    }
    if (length < oldLength) {
        length = oldLength;
        start = oldStart;
    }
    return array.slice(start, start + length);
}