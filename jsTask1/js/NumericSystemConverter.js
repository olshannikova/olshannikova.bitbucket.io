"use strict";
/**
* Numeric systm converter object's constructor
*/
function NumericSystemConverter() {}
/**
* Converts number from binary to decimal
* @param {number[]} number - binary number
* @return {number} decimal number
*/
NumericSystemConverter.binaryToDecimal = function(number) {
    var two = 2;
    var result = number[number.length - 1];
    for (var i = number.length - 2; i >= 0; i--) {
        result += number[i] * two;
        two *= 2;
    }
    return result;
}
/**
* Converts number from decimal to binary
* @param {number[]} number - decimal number
* @return {String} binary number
*/
NumericSystemConverter.decimalToBinary = function(number) {
    var n = number[number.length - 1];
    var ten = 10;
    for (var i = number.length - 2; i >= 0; i--) {
        n += number[i] * ten;
        ten *= 10;
    }
    var result = "";
    while (n > 0) {
        var bit = n % 2;
        result = bit + result;
        n = ~~(n / 2);
    }
    return result;
}