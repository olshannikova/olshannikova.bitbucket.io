"use strict";
/**
* Caching calculator's constructor
*/
function CachingCalculator(cacheSize, isAddCaching, isSubCaching, isMulCaching, isDivCaching) {
    var cache = {
        size: 0
    };
    this.getCache = function() {
        return cache;
    }
    /**
    * Cleans cache if it's full
    */
    function checkAndClearCache() {
        if (cacheSize <= cache.size) {
                cache = {
                    size: 0
                };
            }
    }
    this.add = function f(a, b) {
        if (isAddCaching === false) {
            return a + b;
        }
        if (cache[a + "+" + b] || cache[b + "+" + a]) {
                return cache[a + "+" + b] || cache[b + "+" + a];
        }
        checkAndClearCache();
        cache[a + "+" + b] = a + b;
        cache.size += 1;
        return cache[a + "+" + b];
    }
    this.sub = function f(a, b) {
        if (isSubCaching === false) {
            return a - b;
        }
        if (cache[a + "-" + b]) {
                return cache[a + "-" + b];
        }
        checkAndClearCache();
        cache[a + "-" + b] = a - b;
        cache.size += 1;
        return cache[a + "-" + b];
    }
    this.mul = function f(a, b) {
        if (isMulCaching === false) {
            return a * b;
        }
        if (cache[a + "*" + b] || cache[b + "*" + a]) {
                return cache[a + "*" + b] || cache[b + "*" + a];
        }
        checkAndClearCache();
        cache[a + "*" + b] = a * b;
        cache.size += 1;
        return cache[a + "*" + b];
    }
    this.div = function f(a, b) {
        if (isDivCaching === false) {
            return a / b;
        }
        if (cache[a + "/" + b]) {
                return cache[a + "/" + b];
        }
        checkAndClearCache();
        cache[a + "/" + b] = a / b;
        cache.size += 1;
        return cache[a + "/" + b];
    }
}